import pytest

from tgintegration import BotController, InvalidResponseError


@pytest.mark.asyncio
async def test_run_example(controller: BotController, client):
    await controller.clear_chat()

    async with controller.collect(count=1) as response:  # type: Response
        await controller.send_command("start")

    assert response.num_messages == 1