import asyncio
import logging
from pathlib import Path

import pytest
from config import bot_name

from tgintegration import BotController

examples_dir = Path(__file__).parent.parent

logger = logging.getLogger("tgintegration")
logger.setLevel(logging.DEBUG)
logging.basicConfig(level=logging.DEBUG)

@pytest.fixture(scope="module")
async def controller(client):
    c = BotController(
        client=client,
        peer=bot_name,
        #peer="@BotListBot",
        max_wait=10.0,
        wait_consecutive=0.8,
    )
    await c.initialize(start_client=False)
    yield c
