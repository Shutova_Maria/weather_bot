import requests
import datetime
from config import bot_token, weather_token
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor

bot = Bot(token=bot_token)
dp = Dispatcher(bot)

@dp.message_handler(commands=["start"])
async def start_command(message: types.Message):
    await message.reply("Привет! Погода в каком городе тебя интересует?")

@dp.message_handler()
async def get_weather(message: types.Message):
    code_to_smile = {
        "Clear": "\U00002600 \U00002600 Ясно \U00002600 \U00002600",
        "Clouds": "\U00002601 \U00002601 Облачно \U00002601 \U00002601",
        "Rain": "\U00002614 \U00002614 Дождь \U00002614 \U00002614",
        "Drizzle": "\U00002614 \U00002614 Дождь \U00002614 \U00002614",
        "Thunderstorm": "\U000026A1 \U000026A1 Гроза \U000026A1 \U000026A1",
        "Snow": "\U0001F328 \U0001F328 Снег \U0001F328 \U0001F328",
        "Mist": "\U0001F32B \U0001F32B Туман \U0001F32B \U0001F32B"
    }
    try:
        r = requests.get(
            f"http://api.openweathermap.org/data/2.5/weather?q={message.text}&appid={weather_token}&units=metric"
        )
        data = r.json()


        city = data["name"]
        cur_weather = data["main"]["temp"]
        temp_max = data["main"]["temp"]
        temp_min = data["main"]["temp"]
        weather_description = data["weather"][0]["main"]
        if weather_description in code_to_smile:
            wd = code_to_smile[weather_description]
        else:
            wd = "Посмотри в окно, не пойму, что там за погода!"
        humidity = data["main"]["humidity"]
        pressure = data["main"]["pressure"]
        wind = data["wind"]["speed"]
        sunrise_timestamp = datetime.datetime.fromtimestamp(data["sys"]["sunrise"])
        sunset_timestamp = datetime.datetime.fromtimestamp(data["sys"]["sunset"])
        length_of_the_day = datetime.datetime.fromtimestamp(data["sys"]["sunset"]) - datetime.datetime.fromtimestamp(
            data["sys"]["sunrise"])

        await message.reply(f"\U0001F5D3 {datetime.datetime.now().strftime('%Y-%m-%d %H:%M')} \U0001F5D3"
              f"\U0001F3D9 Погода в городе: {city}\nТемпература: {cur_weather}C° {wd}\nМаксимальная температура сейчас: {temp_max}C°\nМинимальная температура сейчас: {temp_min}C°\n"
              f"\U0001F4A6 Влажность: {humidity}%\n\U0001F321 Давление: {pressure} мм.рт.ст.\n\U0001F4A8 Ветер: {wind} м/с\n"
              f"\U0001F304 Восход солнца: {sunrise_timestamp}\n\U0001F307 Закат солнца: {sunset_timestamp}\n\U000023F3 Продолжительность дня: {length_of_the_day}\n"
              f"\U0001F917 Хорошего дня! \U0001F917"
              )
    except:
        await message.reply("\U00002620 Проверьте название города \U00002620")

if __name__ =='__main__':
    executor.start_polling(dp)
